/*
  Class Juego / Proyecto Ajedrez
  Esteban Arias / Juan Pablo Delgado / Angel Rodriguez 
*/
import javax.swing.JOptionPane; 

public class Juego{

public static void main(String args[]){
	
	Tablero tablero = null;	
	Pieza piezas[] = new Pieza[32];
	
	tablero = new Tablero(); //Aqui se crea el Tablero de 8x8
	new TableroGrafico();
	
	
	/*int cantidadCaballo=0;
	int cantidadAlfil=0;
	int cantidadPeon=0;
	int cantidadRey=0;
	int cantidadReina=0;
	int cantidadTorre=0;
	int ninguna=0;*/
	int indice;
	
	tablero.llenarTablero();			    	    
	    
	JOptionPane.showMessageDialog(null,tablero.toString());	  
	
	
	//Prueba para mover piezas ------------------------------------
	
	int filaActual = Integer.parseInt(JOptionPane.showInputDialog("En cual fila esta la pieza que desea mover (Rey blanco 0,3)"));
	int columnaActual = Integer.parseInt(JOptionPane.showInputDialog("En cual columna esta la pieza que desea mover (Rey blanco 0,3)"));
	
	JOptionPane.showMessageDialog(null,"Se selecciono: "+tablero.getPieza(filaActual, columnaActual).toString());
	
	int filaDestino = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la fila a la que desea mover al rey blanco"));
	int columnaDestino = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la columna a la que desea mover al rey blanco"));
	//int columnaDestino = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la columna a la que desea mover al rey blanco"));
	
	
	if(tablero.getPieza(filaActual,columnaActual).mover(filaActual,columnaActual,filaDestino,columnaDestino, tablero.getPieza(filaActual,columnaActual).getColor(), tablero.getPieza(filaDestino, columnaDestino).getColor())){ //Esto analiza si el movimiento de la pieza es valido 			
		
	tablero.setPieza(filaDestino,columnaDestino, tablero.getPieza(filaActual,columnaActual));
	
	tablero.getPieza(filaActual,columnaActual).setFila(filaDestino);
	tablero.getPieza(filaActual,columnaActual).setColumna(columnaDestino);
		
	tablero.setPieza(filaActual,columnaActual,null);
	
	JOptionPane.showMessageDialog(null,tablero.toString());	
   
   }else{
	   JOptionPane.showMessageDialog(null,"Movimiento invalido");
	    }
	
	
/*	Pieza caballo = new Caballo("Caballo", "Blanco"); //Las nuevas instancias se hacen a partir de las sub-clases
	Pieza peon = new Peon("Peon","Negro");
	Pieza alfil = new Alfil("Alfil","Blanco");
//	Pieza pieza = new Pieza();  -------------Esto ya no se puede hacer por que la clase pieza es abstracta (no se pueden hacer 'new')*/



/*	
	System.out.println("Soy "+piezas[0].toString()+" y "+piezas[0].mover());
	System.out.println("Soy "+piezas[1]+" y "+piezas[1].mover());
	System.out.println("Soy "+piezas[2]+" y "+piezas[2].mover());
	System.out.println("Soy "+piezas[3]+" y "+piezas[3].mover());
	System.out.println("Soy "+piezas[4]+" y "+piezas[4].mover());
	System.out.println("Soy "+piezas[5]+" y "+piezas[5].mover());
	
	for(int indice=0; indice<piezas.length; indice++){
		if(piezas[indice] instanceof Caballo){ //Verifica si en esa posicion hay un objeto de caballo. Verifica cual es el tipo de la instancia 
			cantidadCaballo++;
		}else{
			if(piezas[indice] instanceof Alfil){ //'instanceof' Determina que tipo de instancia hay en ese espacio del array
				cantidadAlfil++;
				}else{
					if(piezas[indice] instanceof Peon){
						cantidadPeon++;
						}else{
							if(piezas[indice] instanceof Rey){
								cantidadRey++;
								}else{
									 if(piezas[indice] instanceof Reina){
										 cantidadReina++;
										}else{
											 if(piezas[indice] instanceof Torre){
												 cantidadTorre++;
												}else{
													 ninguna++;
												}//fin else
													  
										}//Fin else
											  
								}//fin else
							
						}//Fin else
						
				}//Fin else
				
		}//Fin else	
			
	}//Fin for
		
		System.out.println("Hay "+cantidadCaballo+" caballos. "+
		                   cantidadAlfil+" alfiles. "+ cantidadPeon+" peones. "+cantidadRey+" reyes. "+cantidadReina+" reinas. "+
		                   cantidadTorre+" torres. "+ninguna +" de ninguna"); */
		                   
		                   		                 
}//Fin main

}//FIn class
