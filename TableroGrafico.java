/*
  Class TableroGrafico / Proyecto Ajedrez
  Esteban Arias / Juan Pablo Delgado / Angel Rodriguez
*/
import java.awt.Image;
import java.awt.Graphics;
import java.awt.Container;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class TableroGrafico extends JFrame implements MouseListener{
	
	int coordenadaX1;
	int coordenadaX2;
	int coordenadaY1;
	int coordenadaY2;
	
	public TableroGrafico(){
		super("Juego Ajedrez");
		setLayout(new FlowLayout());
		setSize(800, 800);
		Container contenedor = getContentPane();
		contenedor.setBackground(new Color(10, 9, 99));
		addMouseListener(this);
		setVisible(true);
		}//Fin constructor
		
	public void mouseClicked(MouseEvent event){
		System.out.println("Presiono el mouse  en las coordenadas x "+event.getX()+" y "+event.getY()); //Esto me devuelve las coordenadas x y y de donde estamos presionando 
		}
		
	public void mousePressed(MouseEvent event){
		 coordenadaX1 = event.getX();
		 coordenadaY1 = event.getY();
		System.out.println("Presiono el mouse");
		}
		
	public void mouseReleased(MouseEvent event){
		 coordenadaX2 = event.getX();
		 coordenadaY2 = event.getY();
		System.out.println("Dejo de presionar el mouse");
		}	
		
	public void mouseEntered(MouseEvent event){
		System.out.println("Entro el mouse al frame");
		}	
		
	public void mouseExited(MouseEvent event){
		System.out.println("Salio el mouse");
		}		
		
	public void paint(Graphics g){
		super.paintComponents(g);
		Image imagenFondo = new ImageIcon("DisenoPiezas/tablero.png").getImage(); //Esto me imprime el tablero grafico
		g.drawImage(imagenFondo, 0,0, getWidth(), getHeight(), null);
		
		Image imagenReyB = new ImageIcon("DisenoPiezas/reyB.png").getImage(); //Esto me imprime el reyB 
		g.drawImage(imagenReyB, coordenadaX2,coordenadaY2, 31, 75, null); //------------------------------------------------- Es algo asi?
		//g.drawImage(imagenReyB, 338,54, 31, 75, null);
		
		Image imagenReinaB = new ImageIcon("DisenoPiezas/reinaB.png").getImage(); //Esto me imprime el reinaB 
		g.drawImage(imagenReinaB, 416,54, 31, 75, null);
		
		Image imagenAlfilB = new ImageIcon("DisenoPiezas/alfilB.png").getImage(); //Esto me imprime el alfilB 
		g.drawImage(imagenAlfilB, 510,54, 31, 75, null);
		
		Image imagenAlfilB2 = new ImageIcon("DisenoPiezas/alfilB.png").getImage(); //Esto me imprime el alfilB2 
		g.drawImage(imagenAlfilB2, 250,54, 31, 75, null);
		
		
		}				
		
						
	
}//Fin class
