/*
  Class Tablero / Proyecto Ajedrez
  Esteban Arias / Juan Pablo Delgado / Angel Rodriguez
*/
public class Tablero{
	
	private Pieza piezas[][];  
	private Pieza piezaComida; 
	
    public Tablero(){
	piezas = new Pieza[8][8]; //Se crea la matriz de piezas del tamaño del ajedrez (64 casillas)
	}//Fin constructor sin parametros
	
	
	
    public void llenarTablero(){
	 
	setPieza(0,0, new Torre("Torre", true, 0, 0)); //Se le asigna la posicion 0,0 a la torre blanca 1
	setPieza(0,1, new Caballo("Caballo", true, 0, 1));
	setPieza(0,2, new Alfil("Alfil", true, 0, 2));
	setPieza(0,3, new Rey("Rey", true, 0, 3));
	setPieza(0,4, new Reina("Reina", true, 0, 4));
	setPieza(0,5, new Alfil("Alfil", true, 0, 5));
	setPieza(0,6, new Caballo("Caballo", true, 0, 6));
	setPieza(0,7, new Torre("Torre", true, 0, 7)); 
	
    for(int indice=0; indice<length(); indice++){
		setPieza(1,indice, new Peon("Peon", true, 1,indice)); //Se le asigna la pieza de peon blanco a todas los espacios de la fila 2
	    }//fin for
	    
	       
	setPieza(7,0, new Torre("Torre", false,7, 0)); //Se le asigna la posicion 7,0 a la torre negra 1
	setPieza(7,1, new Caballo("Caballo", false,7, 1));
	setPieza(7,2, new Alfil("Alfil", false,7, 2));
	setPieza(7,3, new Rey("Rey", false, 7, 3));
	setPieza(7,4, new Reina("Reina", false, 7, 4));
	setPieza(7,5, new Alfil("Alfil", false,7, 5));
	setPieza(7,6, new Caballo("Caballo", false,7, 6));
	setPieza(7,7, new Torre("Torre", false,7, 7));
	
    for(int indice=0; indice<length(); indice++){
		setPieza(6,indice, new Peon("Peon", false, 1,indice)); //Se le asigna la pieza de peon negro a todas las casillas de la fila 7
	    }//fin for	     
	 
	 }//Fin llenar tablero	
	 
	 	
	
    public void setPieza(int fila, int columna,Pieza pieza){
	piezas[fila][columna]=pieza;	
	}//Fin setPieza	
	
    public Pieza getPieza(int fila, int columna){
	return piezas[fila][columna];
	}//Fin getPieza	
	
    public String toString(){ //Esto me devuelve todos los tipos de pieza que hay en el Tablero en sus posiciones 
	String tipos ="";
	for(int indiceFila =0; indiceFila<piezas.length; indiceFila++){
		for(int indiceColumna =0; indiceColumna<piezas[indiceFila].length; indiceColumna++){
		tipos+=piezas[indiceFila][indiceColumna]+" \t";
		 }
		 tipos += "\n";
	 }
	return tipos;
	}	
	
    public int length(){
	return piezas.length;
	}//Fin length	
	
    public void setPiezaComida(Pieza pieza){
	Pieza piezaComida = pieza;
	}
	
    public Pieza getPiezaComida(){ //Se puede hacer un array de piezas comidas
	return piezaComida;
	}			
	
}//Fin class
