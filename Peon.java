/*
  Class Peon / Proyecto Ajedrez 
  Esteban Arias / Juan Pablo Delgado / Angel Rodriguez
*/
public class Peon extends Pieza{ 
	
	
	public Peon(String tipo, boolean color, int fila, int columna){
		super(tipo,color,fila,columna); //Super invoca al metodo constructor de la Super-Clase (Pieza) El metodo que invoco puede cambiar dependiendo de los parametros
		}//Fin metodo constructor
		
	public boolean mover(int filaActual, int columnaActual, int filaDestino, int columnaDestino, boolean colorInicial, boolean colorFinal){
		
		return true;
		}//Fin mover			

}//Fin class
