/*
  Class Rey / Proyecto Ajedrez
  Esteban Arias / Juan Pablo Delgado / Angel Rodriguez  
*/
public class Rey extends Pieza{
	
	
	public Rey(String tipo, boolean color, int fila, int columna){
		super(tipo, color, fila, columna);
		}//Fin metodo constructor
		
	public boolean mover(int filaActual, int columnaActual, int filaDestino, int columnaDestino, boolean colorInicial, boolean colorFinal){ //Esto controla las restricciones de la pieza. Si es true, se puede mover 
	
		int movimientoFila=(filaActual-filaDestino); 
		int movimientoColumna=(columnaActual-columnaDestino);
		boolean movida = false;
		
	    if(colorInicial != colorFinal){ //Revisa el color de las piezas 
			
		if(movimientoFila==0 && movimientoColumna==1 || movimientoFila==0 && movimientoColumna==-1){ //Si la pieza se mueve uno a la derecha o a la izquierda
			 movida = true;
	    }else{
			if(movimientoColumna==0 && movimientoFila==1 || movimientoColumna==0 && movimientoFila==-1){ //Si le pieza se mueve uno arriba o uno abajo...
				movida = true;
			}else{
				if(movimientoFila==1 && movimientoColumna==1 || movimientoFila==-1 && movimientoColumna==-1){ //Si se mueve en diagonal "\"
					movida = true;
				}else{
					if(movimientoFila==-1 && movimientoColumna==1 || movimientoFila==-1 && movimientoColumna==1){ //Si se mueve en diagonal "/"
						movida = true;
					}else{
						movida = false;
				    }
			    }
			}	
					 
		}
		
	}	 								
		return movida;       
	}//Fin mover	
    
}//Fin class
 
