/*
  Class Pieza / Proyecto Ajedrez
  Esteban Arias / Juan Pablo Delgado / Angel Rodriguez 
*/
import javax.swing.ImageIcon;

public abstract class  Pieza{ //Siempre que declaro un metodo abstracto, la clase tiene que ser abstracta. Al ser abstract no se pueden crear instancias de esa clase pero si arreglos
	private String tipo;
	private boolean color;
	private int fila;
	private int columna;
	private ImageIcon imagen;
	private int x;
	private int y;
	
	public Pieza(String tipo, boolean color, int fila, int columna){//ImageIcon imagen, , int x, int y 
		this.tipo=tipo;
		this.color=color;
		this.fila=fila;
		this.columna=columna;
		/*this.imagen=imagen;
		this.x=x;
		this.y=y;*/
		}//Fin construtor con parametros
		
	public void setFila(int filaDestino){
		fila=filaDestino;
		}//Fin setFila destino		
		
	public int getFila(){
		return fila;
		}//Fin getFila
		
	public void setColumna(int columnaDestino){
		columna=columnaDestino;
		}//Fin setColumnaDestino	
		
	public int getColumna(){
		return columna;
		}//Fin getColumna	
		
	public void setTipo(String tipo){
		this.tipo=tipo;
		}//Fin setTipo	
		
	public String getTipo(){
		return tipo;
		}//Fin getTipo
		
	public void setColor(boolean color){ 
		this.color=color;
		}//Fin setColor	
		
	public boolean getColor(){
		return color;
		}//Fin getColor	
		
	public void setX(int x){
		this.x=x;
		}
		
	public int getX(){
		return x;
		}	

	public void setY(int y){
		this.y=y;
		}				
		
	public int getY(){
		return y;
		}
		
	public void setImageIcon(ImageIcon imagen){
		this.imagen=imagen;
		}	
		
	public ImageIcon getImageIcon(){
		return imagen;
		}						
		
	public String toString(){ //Aqui se define el color de las piezas, puede ser cualquiera.
		String colores;
		if(color){
			     colores = "Blanco";
			     }
			     else{
					 colores = "Negro";
					 }
		return tipo+" "+colores; //Se imprime el tipo de pieza y el color definido
		}//Fin toString  		
		
	public abstract boolean mover(int filaActual, int columnaActual, int filaDestino, int columnaDestino, boolean colorInicial, boolean colorFinal); //Este metodo solo es abstracto en la Super Clase, tiene el cuerpo vacio por que las sub-clases lo llenan
		
		
			
}//Fin class
